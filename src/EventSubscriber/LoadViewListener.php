<?php

namespace Drupal\load_view\EventSubscriber;

use Drupal\Core\Render\RendererInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\views\Views;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LoadViewListener.
 */
class LoadViewListener implements EventSubscriberInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Constructs a new LoadViewListener object.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * This method is called whenever the kernel.request event is dispatched.
   *
   * If a node is requested with 3 additional arguments:
   * 'path_to_nid', 'views_id' and 'display_id'
   * then request return not node, but rendered view, filtered by node id.
   * In this case is used a views display specified in 'display_id' argument.
   * The example of URL:
   * /node_alias?path_to_nid=views&views_id=my_view&display_id=my_views_dispay.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The event to process.
   */
  public function onKernelRequest(GetResponseEvent $event) {
    $request = $event->getRequest();
    $node = $request->attributes->get('node');
    if (!$node) {
      return;
    }
    $path_to_nid = $request->query->get('path_to_nid');
    $views_id = $request->query->get('views_id');
    $display_id = $request->query->get('display_id');

    if (($path_to_nid == 'views') && $views_id && $display_id) {
      $view = Views::getView($views_id);
      if (is_object($view) && $view->access($display_id)) {
        $args = [$node->id()];
        $render_array = $view->buildRenderable($display_id, $args);
        $rendered = $this->renderer->renderRoot($render_array);

        $response = new Response($rendered);
        $event->setResponse($response);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => 'onKernelRequest',
    ];
  }

}
